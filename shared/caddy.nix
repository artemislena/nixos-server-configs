{ lib, ... }:

{
  services.caddy = {
    enable = true;
    logFormat = ''
      level WARN
      exclude http.handlers.reverse_proxy
      format filter {
        wrap console
        fields {
          request delete
        }
      }
    '';
    email = "nontrivialwoolsaw@artemislena.eu";
    globalConfig = ''
      admin unix//run/caddy/admin
      grace_period 10s
    '';
  };

  boot.kernel.sysctl = {
    "net.core.rmem_max" = 7500000;
    "net.core.wmem_max" = 7500000;
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];

  systemd = {
    services.caddy.serviceConfig = {
      CapabilityBoundingSet = [ "CAP_NET_BIND_SERVICE" ];
      ExecPaths = [ "/nix/store" ];
      InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" ];
      LockPersonality = true;
      MemoryDenyWriteExecute = true;
      MemoryHigh = "1G";
      MemoryMax = "2G";
      NoExecPaths = [ "/" ];
      ProcSubset = "pid";
      ProtectClock = true;
      ProtectControlGroups = true;
      ProtectHostname = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      ProtectProc = "invisible";
      RemoveIPC = true;
      RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
      RestrictNamespaces = true;
      RestrictRealtime = true;
      RestrictSUIDSGID = true;
      SystemCallArchitectures = "native";
      SystemCallFilter = [
        "@system-service"
        "~@privileged @aio @keyring @memlock @resources @setuid @timer memfd_create" # Needs @ipc
      ];
    };
    tmpfiles.rules = [ "d /run/caddy 0700 caddy caddy" ];
  };
}
