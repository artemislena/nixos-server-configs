{ ... }:

{
  services.openssh = {
    enable = true;
    hostKeys = [ {
      path = "/etc/ssh/ssh_host_ed25519_key";
      type = "ed25519";
    } ];
    settings = {
      PasswordAuthentication = false;
      LogLevel = "ERROR";
      KbdInteractiveAuthentication = false;
      Ciphers = [
        "chacha20-poly1305@openssh.com"
        "aes256-gcm@openssh.com"
      ];
      KexAlgorithms = [
        "sntrup761x25519-sha512@openssh.com"
        "curve25519-sha256"
        "curve25519-sha256@libssh.org"
      ];
    };
    openFirewall = true;
  };
}
