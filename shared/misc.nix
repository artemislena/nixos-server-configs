{ ... }:

{
  services = {
    logrotate.checkConfig = false; # Otherwise complains about "cannot find name for group ID 30000"

    journald.extraConfig = ''
      Storage=volatile
      RuntimeMaxUse=10M
    '';
  };
}
