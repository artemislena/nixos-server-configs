{ ... }:

{
  services = {
    timesyncd.enable = false;
    chrony = {
      enable = true;
      enableNTS = true;
      # Server list from https://gitea.blesmrt.net/mikaela/shell-things/src/branch/master/etc/chrony/sources.d/nts-servers.sources and https://de.wikipedia.org/wiki/Network_Time_Protocol#NTS
      servers = [
        "ptbtime1.ptb.de"
        "ptbtime2.ptb.de"
        "ptbtime3.ptb.de"
        "ptbtime4.ptb.de"
        "ntppool1.time.nl"
        "ntppool2.time.nl"
        "nts.netnod.se"
      ];
      initstepslew.enabled = false; # Deprecated; makestep is preferred
      extraConfig = "makestep 1 3";
    };
  };

  systemd.services.chronyd.serviceConfig = {
    InaccessiblePaths = [ "/sys" ];
    MemoryHigh = "150M";
    MemoryMax = "500M";
  };
}
