{ ... }:

{
  networking = {
    useNetworkd = true;
    dhcpcd.enable = false;
  };
}
