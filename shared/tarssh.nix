{ pkgs, ... }:

{
  networking.firewall.allowedTCPPorts = [ 22 ];

  users = {
    users.tarssh = {
      isSystemUser = true;
      group = "tarssh";
    };
    groups.tarssh = {};
  };

  systemd.services.tarssh = rec {
    description = "A simple SSH tarpit";
    documentation = [ "https://github.com/Freaky/tarssh" ];
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    requires = after;
    serviceConfig = {
      ExecStart = "${pkgs.tarssh}/bin/tarssh -v --disable-log-timestamps --disable-log-ident -u tarssh --chroot /var/empty -l 0.0.0.0:22";

      CapabilityBoundingSet = [ "CAP_NET_BIND_SERVICE" "CAP_SETUID" "CAP_SETGID" "CAP_SYS_CHROOT" ];
      ExecPaths = [ "/nix/store" ];
      InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" ];
      LockPersonality = true;
      MemoryDenyWriteExecute = true;
      MemoryHigh = "10M";
      MemoryMax = "100M";
      NoExecPaths = [ "/" ];
      NoNewPrivileges = true;
      PrivateDevices = true;
      PrivateTmp = true;
      ProcSubset = "pid";
      ProtectClock = true;
      ProtectControlGroups = true;
      ProtectHome = true;
      ProtectHostname = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      ProtectProc = "invisible";
      ProtectSystem = "strict";
      RemoveIPC = true;
      RestrictAddressFamilies = [ "AF_INET" ];
      RestrictRealtime = true;
      RestrictSUIDSGID = true;
      SystemCallArchitectures = "native";
      SystemCallFilter = [
        "@system-service"
        "~@privileged"
        "@setuid chroot"
        "~@aio @ipc @keyring @memlock @resources @timer"
      ];
    };
  };
}
