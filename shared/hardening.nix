{ config, pkgs, lib, ... }:

{
  # Mostly based on https://madaidans-insecurities.github.io/guides/linux-hardening.html

  environment.memoryAllocator.provider = "graphene-hardened";

  boot = {
    kernelPackages = pkgs.linuxKernel.packages.linux_hardened;

    kernel.sysctl = {
      # Prevent information leaks
      "kernel.dmesg_restrict" = 1;
      "kernel.printk" = "3 3 3 3";
      "kernel.kptr_restrict" = 2;

      "kernel.unprivileged_bpf_disabled" = 1;
      "kernel.yama.ptrace_scope" = 3; # Disable ptrace entirely (2 for only for CAP_SYS_PTRACE)
      "kernel.sysrq" = 0; # Disable sysrq key
      "kernel.perf_event_paranoid" = 3; # Require CAP_SYS_ADMIN for performance events
      "net.core.bpf_jit_harden" = 2; # JIT hardening for all users
      "dev.tty.ldisc_autoload" = 0; # disables ldisc autoloading
      "kernel.io_uring_disabled" = 2; # Disables io_uring even for privileged processes
      "vm.unprivileged_userfaultfd" = 0; # Restricts userfaultfd syscall to CAP_SYS_PTRACE

      # Various networking hardening
      "net.ipv4.tcp_rfc1337" = 1;
      "net.ipv4.tcp_sack" = 0;
      "net.ipv4.tcp_dsack" = 0;
      "net.ipv4.tcp_fack" = 0;
      "net.ipv4.tcp_syncookies" = 1;
      "net.ipv4.conf.all.rp_filter" = 1;
      "net.ipv4.conf.default.rp_filter" = 1;
      "net.ipv4.conf.all.accept_redirects" = 0;
      "net.ipv4.conf.default.accept_redirects" = 0;
      "net.ipv4.conf.all.secure_redirects" = 0;
      "net.ipv4.conf.default.secure_redirects" = 0;
      "net.ipv6.conf.all.accept_redirects" = 0;
      "net.ipv6.conf.default.accept_redirects" = 0;
      "net.ipv4.conf.all.send_redirects" = 0;
      "net.ipv4.conf.default.send_redirects" = 0;
      "net.ipv4.conf.all.accept_source_route" = 0;
      "net.ipv4.conf.default.accept_source_route" = 0;
      "net.ipv6.conf.all.accept_source_route" = 0;
      "net.ipv6.conf.default.accept_source_route" = 0;

      "vm.max_map_count" = 1048576; # For hardened_malloc

      # Maximize ASLR entropy
      "vm.mmap_rnd_bits" = 32;
      "vm.mmap_rnd_compat_bits" = 16;

      # FS hardening
      "fs.protected_symlinks" = 1;
      "fs.protected_hardlinks" = 1;
      "fs.protected_fifos" = 2;
      "fs.protected_regular" = 2;

      # Prevent core dumps
      "kernel.core_pattern" = "|${pkgs.coreutils-full}/bin/false";
      "fs.suid_dumpable" = 0;

      "kernel.deny_new_usb" = 1; # Feature of linux_hardened

      # Panic on IO and memory errors
      "kernel.panic_on_io_nmi" = 1;
      "kernel.panic_on_unrecovered_nmi" = 1;
    };

    kernelParams = [
      "slab_nomerge" # Makes heap exploitation harder
      "init_on_alloc=1" "init_on_free=1"
      "pages_alloc.shuffle=1" # Randomizes page allocation
      "randomize_kstack_offset=on"
      "vsyscall=none"
      "debugfs=off"
      "oops=panic"
      "module.sig_enforce=1"
      "lockdown=confidentiality" # Prevents some attacks by privileged processes
      "mce=0" # Panic on uncorrectable ECC errors
      "iommu=force" "intel_iommu=on" "amd_iommu=force_isolation" "iommu.passthrough=0" "iommu.strict=1" "efi=disable_early_pci_dma" # Prevent DMAs
      "mitigations=auto"
      "quiet" "loglevel=0"
      "random.trust_bootloader=off" # Don't use bootloader seed file
    ];

    blacklistedKernelModules = [
      "dccp" "sctp" "rds" "tipc" "n-hdlc" "ax25" "netrom" "x25" "rose" "decnet" "econet" "af_802154" "ipx" "appletalk" "psnap" "p8023" "p8022" "can" "atm" # Network protocols
      "cramfs" "freevxfs" "jffs2" "hfs" "hfsplus" "squashfs" "udf" # File systems
      "cifs" "nfs" "nfsv3" "nfsv4" "ksmbd" "gfs2" # Network file systems
      "vivid" # Debug/testing driver
      "bluetooth" "btusb"
      "uvcvideo" # Webcam
    ];

    tmp.useTmpfs = true;
  };

  security = {
    sudo.enable = false;
    polkit.enable = false;
    apparmor = {
      enable = true;
      packages = with pkgs; [ apparmor-profiles ];
      killUnconfinedConfinables = true;
    };
    protectKernelImage = true;
    forcePageTableIsolation = true;
    lockKernelModules = true;
    allowSimultaneousMultithreading = false;
    virtualisation.flushL1DataCache = "always";
  };

  services = {
    dbus.apparmor = "enabled";
    udisks2.enable = false;
  };

  fileSystems = {
    "/boot" = {
      device = lib.mkDefault "/boot";
      options = [ "nosuid" "nodev" "noexec" "noatime" ] ++ ( if config.fileSystems."/boot".fsType == "vfat" then [ "umask=777" ] else [ "bind" ] );
    };
    "/proc" = {
      device = "proc";
      fsType = "proc";
      options = [ "nosuid" "nodev" "noexec" "hidepid=2" "gid=viewproc" ];
    };
  };
  users.groups.viewproc = {};
  systemd.services.systemd-logind.serviceConfig.SupplementaryGroups = [ "viewproc" ];
}
