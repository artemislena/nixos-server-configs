{ config, pkgs, lib, ... }:

{
  # Credits to https://xeiaso.net/blog/borg-backup-2021-01-09 for the below
  services.borgbackup.jobs.backup = {
    paths = [
      "/var/lib"
      "/etc"
      "/root/.ssh" "/root/.config/borg"
    ];
    exclude = [
      "/var/lib/systemd"
      "/var/lib/nixos"
      "/var/lib/machines"
      "/var/lib/logrotate.status"
      "/var/lib/portables"
      "/var/lib/chrony"
    ];
    encryption = {
      mode = lib.mkDefault "repokey";
      passCommand = "cat /root/.borg-passphrase";
    };
    # https://github.com/borgbackup/borg/issues/1040 suggests compression+encryption is a non-issue in most cases, but attacks might be possible (if poorly feasible) if attackers can insert data to a DB or similar, so we rather disable it for servers, which might also save a bit of CPU load (and bandwidth-wise isn't a large issue on a LAN)
    compression = "none";
    startAt = lib.mkDefault "04,12,20:00";
    prune.keep = {
      within = "1d";
      daily = 2;
    };
    persistentTimer = true;
    environment.BORG_RELOCATED_REPO_ACCESS_IS_OK = "yes"; # Mostly annoying otherwise; prevents using repo if backup server address changed

    postHook = ''
      if [ "$exitStatus" == '0' ]; then
        ${pkgs.curl}/bin/curl -s -u "$(cat /etc/borg-ntfy-auth)" -H 't:${config.networking.hostName}' -H 'p:min' -H 'ta:white_check_mark' -d 'Backup successful.' 'https://ntfy.artemislena.eu/borg'
      else
        ${pkgs.curl}/bin/curl -s -u "$(cat /etc/borg-ntfy-auth)" -H 't:${config.networking.hostName}' -H 'p:high' -H 'ta:warning' -d 'Backup or integrity check failed.' 'https://ntfy.artemislena.eu/borg'
      fi
    '';

    # https://bsd.network/@solene/108900687372607331
    postPrune = ''
      borgWrapped check $extraArgs --repository-only --max-duration 300
      borgWrapped check $extraArgs --archives-only
    '';
  };

  systemd = {
    services.borgbackup-job-backup.serviceConfig = {
      ExecPaths = [ "/nix/store" ];
      InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" ];
      CapabilityBoundingSet = [ "CAP_DAC_READ_SEARCH" ];
      LockPersonality = true;
      MemoryDenyWriteExecute = true;
      NoExecPaths = [ "/" ];
      NoNewPrivileges = true;
      PrivateDevices = true;
      ProcSubset = "pid";
      ProtectClock = true;
      ProtectControlGroups = true;
      ProtectHostname = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      ProtectProc = "invisible";
      RemoveIPC = true;
      RestrictAddressFamilies = [ "AF_INET" "AF_INET6" ];
      RestrictRealtime = true;
      RestrictNamespaces = true;
      RestrictSUIDSGID = true;
      SystemCallArchitectures = "native";
      SystemCallFilter = [
        "@system-service"
        "~@privileged @aio @keyring @memlock @resources @setuid @timer memfd_create" # Needs @ipc
      ];
    };

    timers.borgbackup-job-backup = {
      wants = [ "network-online.target" ]; # Temporary hack because upstream broke this
      timerConfig.RandomizedDelaySec = lib.mkDefault "45min";
    };
  };

  environment.variables.BORGPASSCOMMAND = config.services.borgbackup.jobs.backup.encryption.passCommand;
}
