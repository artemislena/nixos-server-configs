{ pkgs, ... }:

{
  environment = {
    variables = {
      CLICOLOR = "TRUE";
      EDITOR = "kak";
    };
    defaultPackages = with pkgs; [
      git # Needed by flakes
      kakoune
      tealdeer
      nmap
      bind
      croc
      killall
    ];
  };

  time.timeZone = "Europe/Berlin";

  users.users.root = {
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEa6r9rTXovIjgkbPt5dSp4klJ+oJf294ogkD7pujBK+ 7973D087497ED49AF3DCD66E412A96FD1D74214B"
    ];
  };

  programs = {
    zsh = {
      enable = true;
      setOptions = [ "extendedglob" "no_beep" ];
      syntaxHighlighting.enable = true;
      shellAliases = {
        la = "ls -a";
        ll = "ls -al";
        grep = "grep --color=auto";
      };
      histSize = 50;
      interactiveShellInit = ''
        umask 077

        zstyle ':completion:*' completer _expand _complete _ignored _match _approximate _prefix
        zstyle ':completion:*' ignore-parents parent pwd; zstyle ':completion:*' insert-unambiguous true; zstyle ':completion:*' list-colors ""
        zstyle ':completion:*' menu select=1
        zstyle ':completion:*' original true
        zstyle ':completion:*' use-compctl false
        bindkey -v

        function checkfile {
          if [ -f "$1" ]; then
            stat -c '%A' $1 | grep $2 >/dev/null
          else
            echo "Warning. $1 doesn't exist or has incorrect permissions."
          fi
        }
        test "$USER" '==' 'root' && checkfile /root/.borg-passphrase '\----------'
        checkfile /etc/borg-ntfy-auth '\-r*-*-----'
      '';
    };

    ssh = {
      ciphers = [
        "chacha20-poly1305@openssh.com"
        "aes256-gcm@openssh.com"
      ];
      kexAlgorithms = [ "sntrup761x25519-sha512@openssh.com" ];
      knownHosts.onyx = {
        extraHostNames = [ "borg.artemislena.eu" "192.168.0.10" ];
        publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOfydibZ7LEgmzwoV9aJx2vUfQvsaa4Gdsew/oiIGFPO";
      };
    };
  };
}
