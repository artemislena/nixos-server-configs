{ config, pkgs, ... }:

{
  users = {
    users.jsonfeed = {
      home = "/home/jsonfeed";
      isNormalUser = true;
      group = "jsonfeed";
    };
    groups.jsonfeed = {};
  };

  environment.systemPackages = with pkgs; [ (python3.withPackages(ps: [ps.matrix-nio])) ];

  services.borgbackup.jobs.backup.paths = [ "/home" ];

  systemd.services.jsonfeed = rec {
    description = "Matrix bot for JSON feeds";
    wantedBy = [ "multi-user.target" ];
    requires = [ "caddy.service" "matrix-synapse.service" ];
    after = requires;
    documentation = [ "https://codeberg.org/artemislena/jsonfeed-bot" ];
    serviceConfig = rec {
      User = "jsonfeed";
      Group = "jsonfeed";
      ExecStart = with pkgs; "${(python3.withPackages(ps: [ps.matrix-nio]))}/bin/python3 /etc/nixos/jsonfeed-bot/jsonfeed-bot.py";
      WorkingDirectory = "/home/jsonfeed";
      Restart = "on-failure";
      RestartSec = "1m";
      BindPaths = [ "/home/jsonfeed/latest" ];
      BindReadOnlyPaths = [ WorkingDirectory ];
      CapabilityBoundingSet = [ "" ];
      ExecPaths = [ "/nix/store" ];
      InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" config.microvm.stateDir ];
      LockPersonality = true;
      MemoryDenyWriteExecute = true;
      MemoryHigh = "150M";
      MemoryMax = "500M";
      NoExecPaths = [ "/" ];
      NoNewPrivileges = true;
      PrivateDevices = true;
      PrivateTmp = true;
      PrivateUsers = true;
      ProcSubset = "pid";
      ProtectClock = true;
      ProtectControlGroups = true;
      ProtectHome = "tmpfs";
      ProtectHostname = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      ProtectProc = "invisible";
      ProtectSystem = "strict";
      RemoveIPC = true;
      RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
      RestrictNamespaces = true;
      RestrictRealtime = true;
      RestrictSUIDSGID = true;
      SystemCallArchitectures = "native";
      SystemCallFilter = [
        "@system-service"
        "~@privileged @aio @ipc @keyring @memlock @resources @setuid @timer memfd_create"
      ];
    };
  };
}
