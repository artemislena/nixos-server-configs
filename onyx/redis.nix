{ config, pkgs, ... }:

{
  services.redis = {
    vmOverCommit = true;
    package = pkgs.valkey;
    servers = let redisConfig = instanceName: {
      enable = true;
      logLevel = "warning";
      settings.include = "/etc/redis/redis-${instanceName}_acl.conf";
    }; in {
      authelia = {
        enable = true;
        logLevel = "warning";
        user = "authelia";
        unixSocketPerm = 600;
        settings.user = [ "default on nopass +@all -@admin -@dangerous ~* &*" ];
      };
      ffsend = redisConfig "ffsend" // {
        port = 6382;
        bind = null; # Listen on all interfaces because Podman interface might not be up when Redis starts
      };
      matrix-synapse = {
        logLevel = "warning";
        settings.user = [ "default on nopass +@all -@admin -@dangerous ~* &*" ];
      };
      proxitok = redisConfig "proxitok" // {
        unixSocketPerm = 666;
        save = [ [ 60 1 ] ]; # https://github.com/pablouser1/ProxiTok/blob/master/docker-compose.yml
      };
    };
  };

  systemd.services = let redisConfig = {
    Restart = "on-failure";
    ExecPaths = [ "/nix/store" ];
    InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" "/etc/ld-nix.so.preload" config.microvm.stateDir ]; # Has a memory bug that hardened_malloc (and scudo) catches
    MemoryHigh = "100M";
    MemoryMax = "200M";
    NoExecPaths = [ "/" ];
    ProcSubset = "pid";
    ProtectProc = "invisible";
    RemoveIPC = true;
  }; in {
    redis-authelia.serviceConfig = redisConfig;
    redis-ffsend.serviceConfig = redisConfig;
    redis-matrix-synapse.serviceConfig = redisConfig;
    redis-proxitok.serviceConfig = redisConfig;
  };
}
