{ config, ... }:


{
  services.tor = {
    enable = true;
    enableGeoIP = false;
    settings = {
      Sandbox = true;
      HardwareAccel = true;
      AvoidDiskWrites = true;
      NoExec = true;
      HiddenServiceNonAnonymousMode = true;
      HiddenServiceSingleHopMode = true;
    };
    relay.onionServices = let onion-www = {
      map = [ { port = 80; } ];
    }; in {
      "artemislena.eu" = onion-www;
      gtrr = onion-www;
    };
  };

  systemd.services.tor.serviceConfig = {
    ExecPaths = [ "/nix/store" ];
    InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" config.microvm.stateDir ];
    MemoryHigh = "150M";
    MemoryMax = "500M";
    NoExecPaths = [ "/" ];
    SystemCallFilter = [ "~memfd_create" ];
  };
}
