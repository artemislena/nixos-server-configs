{ pkgs, ... }:

{
  imports = [
    ./acme.nix
    ./caddy
    ./containers
    ./invidious.nix
    ./ip-change.nix
    ./jsonfeed.nix
    ./microvms
    ./ntfy.nix
    ./postgres.nix
    ./redis.nix
    ./shared/tarssh.nix
    ./synapse
    ./syncplay.nix
    ./tor.nix
    ./borg-repos.nix # May be world-readable with access to the host, but not published in this repo anyway for privacy reasons, note Borg users can ask us to see the config for their own repo
    ./friend-backups.nix # Same here
  ];

  networking = {
    hostName = "onyx";
    firewall = {
      allowedTCPPorts = [
        8448 # Synapse
        8999 # SyncPlay
      ];
      allowedUDPPorts = [
        443 # HTTP/3
        8448 # Matrix
      ];
    };
  };


  # SSD options
  boot.initrd.luks.devices = rec {
    cryptroot = {
      bypassWorkqueues = true;
      allowDiscards = true;
    };
    "cryptroot-2" = cryptroot;
  };

  swapDevices = let
    standardSwap = partuuid: [ {
      device = "/dev/disk/by-partuuid/${partuuid}";
      options = [ "nofail" ];
      randomEncryption = {
        enable = true;
        allowDiscards = true;
      };
    } ];
  in
  standardSwap "605e7c6a-a7be-4a0d-bb9b-6a43c55e9764" ++ standardSwap "a6c5c6f4-563d-4033-b4cd-05afec5f8f2b";

  services = {
    btrfs.autoScrub.enable = true;

    borgbackup.jobs.backup = {
      repo = "/var/lib/backups/onyx";
      exclude = [ "/var/lib/backups" ];
      paths = [ "/srv" ];
    };

    openssh.ports = [ 2222 ];
  };

  environment.systemPackages = with pkgs; [ smartmontools lm_sensors rsync python3 ];
  users.defaultUserShell = pkgs.dash; # See man rrsync

  systemd.services.disable-boost = {
    description = "Disable AMD CPU boosting";
    wantedBy = [ "multi-user.target" ];
    script = ''
      echo 0 > /sys/devices/system/cpu/cpufreq/boost
    '';
    serviceConfig.Type = "oneshot";
  };

  system.stateVersion = "24.05"; # Don't change unless you know what you're doing
}
