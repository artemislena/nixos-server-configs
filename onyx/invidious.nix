{ ... }:

{
  users = {
    users.invidious = {
      isSystemUser = true;
      group = "invidious";
    };
    groups.invidious = {};
  };

  services.invidious = {
    enable = true;
    serviceScale = 3;
    port = 3000;
    address = "127.0.0.1";
    settings = {
      external_port = 443;
      domain = "yt.artemislena.eu";
      https_only = true;
      hsts = true;
      disable_proxy = true;
      use_innertube_for_captions = true;
      log_level = "Fatal";
      popular_enabled = false;
      statistics_enabled = true;
      registration_enabled = false;
      admins = [ "artemislena" ];
      enable_user_notifications = false; # Reduces disk usage
      banner = ''Due to us having issues keeping this running and Youtube making it harder and harder to avoid rate limits, this instance is being shut down on June 25th, 2025. If you have an account, please export your data and switch to <a href=/https://docs.invidious.io/instances/">another instance</a> as soon as possible.'';
      use_pubsub_feeds = true;
      default_user_preferences = {
        default_home = "<none>";
        autoplay = true;
        feed_menu = [ "Trending" "Subscriptions" "Playlists" ];
      };
    };
  };
}
