{ config, lib, ... }:

{
  services.ntfy-sh = {
    enable = true;
    settings = rec {
      base-url = "https://ntfy.artemislena.eu";
      listen-http = "";
      listen-unix = "/run/ntfy/socket";
      listen-unix-mode = 666;
      cache-file = "/var/cache/ntfy/cache.db";
      cache-startup-queries = ''
        pragma journal_mode = WAL;
        pragma synchronous = normal;
        pragma temp_store = memory;
        pragma mmap_size = 100000000;
        pragma vacuum;
      '';
      auth-file = "/var/lib/ntfy-sh/user.db";
      auth-default-access = "deny-all";
      auth-startup-queries = cache-startup-queries;
      behind-proxy = true;
      attachment-cache-dir = "/var/cache/ntfy/attachments";
      enable-login = true;
      upstream-base-url = "https://ntfy.sh";
      log-level = "warn";
    };
  };

  systemd.services.ntfy-sh.serviceConfig = rec {
    RuntimeDirectory = [ "ntfy" ];
    CacheDirectory = [ "ntfy" ];

    AmbientCapabilities = lib.mkForce [ "" ];
    CapabilityBoundingSet = AmbientCapabilities;
    ExecPaths = [ "/nix/store" ];
    InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" config.microvm.stateDir ];
    LockPersonality = true;
    MemoryHigh = "150M";
    MemoryMax = "500M";
    NoExecPaths = [ "/" ];
    PrivateUsers = true;
    ProcSubset = "pid";
    ProtectClock = true;
    ProtectHome = true;
    ProtectHostname = true;
    ProtectProc = "invisible";
    ProtectSystem = lib.mkForce "strict";
    RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
    SystemCallArchitectures = "native";
    SystemCallFilter = [
      "@system-service"
      "~@privileged @aio @keyring @memlock @resources @setuid @timer memfd_create" # Needs @ipc
    ];
  };
}
