{ pkgs, ... }:

{
  systemd.services.ip-change = {
    description = "Check if IP addresses changed and take appropriate action";
    startAt = "*:0/5";
    path = with pkgs; [ iproute2 dig curl ];
    environment.MYFRITZ_DOMAIN = "bhz2qrewrokoaom2.myfritz.net";
    serviceConfig = {
      Type = "oneshot";
      EnvironmentFile = "/etc/dns_tokens";
      StandardOutput = "journal";
      DynamicUser = true;
    };
    script = ''
      CURRENT_V4="$(dig +short "$MYFRITZ_DOMAIN" A)" # Kept up to date by FritzBox
      OLD_V4="$(dig +short artemislena.eu A)"
      CURRENT_V6="$(ip -6 address show scope global dynamic mngtmpaddr noprefixroute | grep 'inet6' | cut -d ' ' -f 6 | sed 's#/64##')"
      OLD_V6="$(dig +short artemislena.eu AAAA)"
      if [ "$CURRENT_V4" != "$OLD_V4" ] || [ "$CURRENT_V6" != "$OLD_V6" ]; then
        # deSEC
        curl -X PUT "https://desec.io/api/v1/domains/artemislena.eu/rrsets/" \
        -H "Authorization: Token $DESEC_TOKEN" -H "Content-Type: application/json" \
        -d '[
          {"subname": "", "type": "A", "ttl": 60, "records": ["'"$CURRENT_V4"'"]},
          {"subname": "", "type": "AAAA", "ttl": 60, "records": ["'"$CURRENT_V6"'"]},
          {"subname": "bw", "type": "A", "ttl": 60, "records": ["'"$CURRENT_V4"'"]},
          {"subname": "bw", "type": "AAAA", "ttl": 60, "records": ["'"$CURRENT_V6"'"]},
          {"subname": "syncplay", "type": "A", "ttl": 60, "records": ["'"$CURRENT_V4"'"]},
          {"subname": "syncplay", "type": "AAAA", "ttl": 60, "records": ["'"$CURRENT_V6"'"]}
        ]'
     fi
    '';
  };
}
