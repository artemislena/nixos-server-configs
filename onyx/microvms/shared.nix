{ ... }:

{
  imports = [
    ../shared/borg.nix
    ../shared/environment.nix
    ../shared/hardening.nix
    ../shared/misc.nix
    ../shared/network.nix
    ../shared/ssh.nix
  ];

  microvm = {
    shares = [ {
      tag = "ro-store";
      proto = "virtiofs";
      source = "/nix/store";
      mountPoint = "/nix/.ro-store";
    } ];
  };

  # https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/chap-kvm_guest_timing_management#sect-KVM_guest_timing_management-Host-guest-time-sync
  boot.initrd.kernelModules = [ "ptp_kvm" ]; # boot.kernelModules doesn't seem to work for microVMs
  services = {
    timesyncd.enable = false;
    chrony = {
      enable = true;
      servers = [ ];
      initstepslew.enabled = false;
      extraConfig = "refclock PHC /dev/ptp0 poll 2";
    };
  };
  systemd.services.chronyd.serviceConfig = {
    InaccessiblePaths = [ "/sys" ];
    MemoryHigh = "150M";
    MemoryMax = "500M";
  };
}
