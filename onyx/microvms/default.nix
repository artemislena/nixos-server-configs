{ config, ... }:

{
  imports = [
    ./mailcow
  ];

  boot.kernelModules = [ "ext4" ]; # To mount images when needed

  systemd.network = {
    netdevs.br0.netdevConfig = {
      Name = "br0";
      Kind = "bridge";
      MACAddress = "9c:6b:00:13:2a:73"; # Same as enp6s0
    };

    networks = {
      "10-lan" = {
        matchConfig.Name = [ "enp6s0" "vm-*" ];
        networkConfig.Bridge = "br0";
      };
      "10-lan-bridge" = {
        matchConfig.Name = "br0";
        networkConfig = {
          DHCP = true;
          IPv6AcceptRA = true;
        };
        linkConfig.RequiredForOnline = "routable";
      };
    };
  };

  systemd.services = {
    chrony.serviceConfig.InaccessiblePaths = [ config.microvm.stateDir ];
    tarssh.serviceConfig.InaccessiblePaths = [ config.microvm.stateDir ];
  };
}
