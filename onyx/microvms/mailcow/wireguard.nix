{ ... }:

{
  networking.firewall = {
    extraCommands = ''
      iptables -A FORWARD --in-interface wg1 --out-interface vm-mailcow -j REJECT
      ip6tables -A FORWARD --in-interface wg1 --out-interface vm-mailcow -j REJECT
    '';
    extraStopCommands = ''
      iptables -D FORWARD --in-interface wg1 --out-interface vm-mailcow -j REJECT
      ip6tables -D FORWARD --in-interface wg1 --out-interface vm-mailcow -j REJECT
    '';
  };

  networking.wireguard.useNetworkd = false; # Can't get this working currently

  networking.wireguard.interfaces.wg1 = {
    ips = [ "10.0.1.1/24" "fc00:1::1/64" ];
    privateKeyFile = "/etc/wireguard/private";
    peers = [ {
      publicKey = "Yerh9wI9K8EfIobSSShACFQtYFiBrXwUBqbpO0vK0kA=";
      presharedKeyFile = "/etc/wireguard/psk";
      endpoint = "[2a03:4000:42:19d::2]:5183";
      allowedIPs = [ "0.0.0.0/0" "::/0" ];
      persistentKeepalive = 25;
    } ];
  };
}
