{ config, ... }:

{
  services.borgbackup.jobs.backup.exclude = [ "${config.microvm.stateDir}/mailcow" ];

  microvm.vms.mailcow.config = {
    imports = [
      ../shared.nix
      ./wireguard.nix
      ./mailcow-dockerized.nix
    ];

    microvm = {
      vcpu = 2;
      mem = 3000;
      balloonMem = 3000;
      interfaces = [ {
        type = "tap";
        id = "vm-mailcow";
        mac = "ee:00:00:00:00:01";
      } ];

      shares = [
        {
          proto = "virtiofs";
          tag = "roothome";
          source = "root";
          mountPoint = "/root";
        }
        {
          proto = "virtiofs";
          tag = "etc";
          source = "etc";
          mountPoint = "/etc";
        }
        {
          proto = "virtiofs";
          tag = "mailcow-dockerized";
          source = "mailcow-dockerized";
          mountPoint = "/opt/mailcow-dockerized";
        }
      ];

      # Docker doesn't like virtiofs
      volumes = [ {
          label = "varlib";
          image = "varlib.img";
          mountPoint = "/var/lib";
          size = 40000;
      } ];
    };

    systemd.network.networks."20-lan" = {
      matchConfig.Type = "ether";
      networkConfig.DHCP = true;
      routes = [ {
        Gateway = "_ipv6ra";
        Destination = "2a03:4000:42:19d::2";
      } ];
    };

    networking = {
      hostName = "mailcow";
      firewall.interfaces.wg1.allowedTCPPorts = [ 25 465 587 993 995 4190 80 443 ];
    };

    services.borgbackup.jobs.backup = {
      repo = "ssh://mailcow@192.168.0.10:2222/./";
      paths = [ "/opt/mailcow-dockerized" ];
      exclude = [
        "/var/lib/docker/overlay2"
        "/var/lib/docker/containers"
        "/var/lib/docker/containerd"
        "/var/lib/docker/volumes/mailcowdockerized_crypt-vol-1"
        "/var/lib/docker/volumes/mailcowdockerized_mysql-*vol-1"
        "/var/lib/docker/volumes/mailcowdockerized_postfix-vol-1"
        "/var/lib/docker/volumes/mailcowdockerized_redis-vol-1"
        "/var/lib/docker/volumes/mailcowdockerized_rspamd-vol-1"
        "/var/lib/docker/volumes/mailcowdockerized_vmail-vol-1"
      ];
    };

    system.stateVersion = "25.05"; # Don't change unless you know what you're doing
  };
}
