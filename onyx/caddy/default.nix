{ config, pkgs, ... }:

{
  imports = [ ../shared/caddy.nix ];

  services.caddy.extraConfig = ''
    (no-log) {
      log {
        output discard
      }
    }

    (handle-errors) {
      handle_errors {
        @notfound expression {err.status_code} == 404
        respond @notfound "{err.status_code} {err.status_text}. The link you're trying to access is outdated or never existed in the first place." {err.status_code}

        @badgateway expression {err.status_code} == 502
        respond @badgateway "{err.status_code} {err.status_text}. The backend of this service is currently down, but it will probably be up again soon. Try again later." {err.status_code}

        respond "{err.status_code} {err.status_text}" {err.status_code}
      }
    }

    (no-errors-proxy) {
      reverse_proxy {args[0]} {
        transport http {
          compression off
        }
      }
    }
    (proxy) {
      import no-log
      import no-errors-proxy {args[0]}
      import handle-errors
    }

    (standard-headers-without-hsts) {
      header {
        Referrer-Policy no-referrer
        X-Content-Type-Options nosniff
        -Server
      }
    }
    (hsts) {
      header Strict-Transport-Security "max-age=63072000; includeSubDomains"
    }
    (hsts-preload) {
      header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"
    }
    (standard-headers) {
      import standard-headers-without-hsts
      import hsts
    }
    
    # https://www.permissionspolicy.com
    (no-permissions) {
      header Permissions-Policy "accelerometer=(), autoplay=(), camera=(), clipboard-read=(), clipboard-write=(), cross-origin-isolated=(), display-capture=(), encrypted-media=(), fullscreen=(), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), xr-spatial-tracking=()"
    }
    (allow-media) {
      header Permissions-Policy "accelerometer=(), autoplay=(self), camera=(), clipboard-read=(), clipboard-write=(), cross-origin-isolated=(), display-capture=(), encrypted-media=(), fullscreen=(self), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(self), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), xr-spatial-tracking=()"
    }

    # https://grapheneos.org/articles/sitewide-advertising-industry-opt-out
    (anti-adware) {
      @ads path /ads.txt /app-ads.txt
      respond @ads "placeholder.example.com, placeholder, DIRECT, placeholder"
    }

    # Disincentivizes bots that try to exploit common dynamic code paths (i.e. WordPress logins) by serving them zipbombs or alternatively redirecting them to large downloads
    &(bot-defense) {
      @bot <<CEL
        header_regexp('User-Agent', '(?i)chatglm.spider|chatglm\\.cn|ai[0-9]bot|allenai\\.org')
        || header({'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/605.1.15 (KHTML, like Gecko; compatible; FriendlyCrawler/1.0) Chrome/120.0.6099.216 Safari/605.1.15',
        'User-Agent': 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; PerplexityBot/1.0; +https://docs.perplexity.ai/docs/perplexity-bot)',
        'User-Agent': 'nikto', 'User-Agent': 'sqlmap' })
        || path('*.php', '*.aspx', '/wp/*', '/wordpress*', '/wp-*')
        CEL

      @compression header_regexp Accept-Encoding ^(.*,\s*)?(gzip|br|zstd)(,.*)?$
      route @bot {
        handle @compression {
          rewrite * /bomb
          file_server {
            root /srv/www/bombs
            precompressed br zstd gzip
          }
          header {
            -ETag
            -Server
            Content-Type text/hmtl # Don't make the trick too obvious
            X-Content-Type-Options nosniff
          }
        }
        redir https://hel1-speed.hetzner.com/10GB.bin
      }
    }

    (robots-deny-all) {
      respond /robots.txt <<TXT
        User-agent: *
        Disallow: /
        TXT
    }

    (static-files) {
      import no-log
      invoke bot-defense
      @hashed query h=*

      root * /srv/www/{args[0]}
      encode zstd gzip
      import standard-headers-without-hsts
      header @hashed {
        Cache-Control "max-age=31536000, immutable"
        -ETag
      }
      import handle-errors
    }

    (onion-location) {
      header Onion-Location http://{args[0]}{uri}
    }

    (breezewiki-tls) {
      tls ${config.security.acme.certs."bw.artemislena.eu".directory}/fullchain.pem ${config.security.acme.certs."bw.artemislena.eu".directory}/key.pem {
        protocols tls1.3
      }
    }

    (matrix-client) {
      import anti-adware
      invoke bot-defense
      import no-log
      tls {
        protocols tls1.3
      }
      encode zstd gzip
      import standard-headers
      header Permissions-Policy "accelerometer=(), autoplay=(), camera=(self), clipboard-read=(), clipboard-write=(self), cross-origin-isolated=(), display-capture=(self), document-domain=(), encrypted-media=(), fullscreen=(self), geolocation=(self), gyroscope=(), magnetometer=(), microphone=(self), midi=(), payment=(), picture-in-picture=(self), publickey-credentials-get=(), usb=(), screen-wake-lock=(), sync-xhr=(), xr-spatial-tracking=()"
      handle /config.json {
        rewrite * /{args[1]}
        root * /etc/nixos/matrix-client-configs
      }
      handle {
        root * {args[0]}
      }
      file_server
      import handle-errors
    }

    import /etc/nixos/caddy/config/*

    # Paths for Matrix clients are only accessible from Nix

    element.artemislena.eu {
      @nocache path /index.html /version /config*
      import matrix-client ${pkgs.element-web} element.json
      header Content-Security-Policy "frame-ancestors 'none'"
      header @nocache Cache-Control no-cache
    }

    # https://github.com/NixOS/nixpkgs/pull/334638
    #cinny.artemislena.eu {
    #  import matrix-client $ {pkgs.cinny} cinny.json
    #  header Content-Security-Policy "default-src 'none'; style-src 'self'; font-src 'self' data:; script-src 'self' 'unsafe-eval' 'unsafe-inline'; script-src-attr 'none'; connect-src https:; img-src https: data: blob:; media-src https: blob:; manifest-src 'self'; base-uri 'self'; frame-ancestors 'none'; form-action 'none'"
    #}
  '';

  # https://github.com/NixOS/nixpkgs/pull/334638
  nixpkgs.config.permittedInsecurePackages = [
    "cinny-unwrapped-4.2.3"
    "olm-3.2.16"
  ];

  systemd.services.caddy = {
    environment = {
      ONION = "lpoaj7z2zkajuhgnlltpeqh3zyq7wk2iyeggqaduhgxhyajtdt2j7wad.onion";
      GTRR_ONION = "664bnfuk2m52sdyfq52kzvzbouneltoczw2vcxctiahwphsgnzs22uyd.onion";
    };
    serviceConfig.InaccessiblePaths = [ config.microvm.stateDir ];
  };
}
