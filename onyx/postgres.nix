{ config, pkgs, ... }:

{
  services = {
    postgresql = {
      enable = true;
      package = pkgs.postgresql_16;
      initdbArgs = [
        "--data-checksums"
        "--encoding=UTF8" "--lc-collate=C" "--lc-ctype=C" # Wanted by Synapse and GoToSocial
      ];
      ensureUsers = [
        {
          name = "authelia";
          ensureDBOwnership = true;
        }
        {
          name = "gotosocial";
          ensureDBOwnership = true;
        }
        {
          name = "invidious";
          ensureDBOwnership = true;
        }
        {
          name = "matrix-synapse";
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ "authelia" "gotosocial" "invidious" "matrix-synapse" ];
      settings = {
        password_encryption = "scram-sha-256";
        default_toast_compression = "lz4";
        wal_compression = "zstd";
      };
      authentication = ''
        # connection-type db user [address] method
        local authelia authelia scram-sha-256
        local gotosocial gotosocial scram-sha-256
        host all postgres 0.0.0.0/0 reject
        host all postgres ::/0 reject
        host all all 127.0.0.1/32 scram-sha-256
        host all all ::1/128 scram-sha-256
      '';
    };

    borgbackup.jobs = {
      backup.exclude = [ "/var/lib/postgresql" ];

      postgres = {
        user = "postgres";
        group = "postgres";
        encryption = {
          mode = config.services.borgbackup.jobs.backup.encryption.mode;
          passCommand = "cat %d/passphrase";
        };
        compression = "none";
        prune.keep = config.services.borgbackup.jobs.backup.prune.keep;
        postPrune = config.services.borgbackup.jobs.backup.postPrune;
        startAt = [ ];
        repo = "/var/lib/backups/onyx-postgres";
        dumpCommand = "${config.services.postgresql.package}/bin/pg_dumpall";
        postHook = ''
          if [ "$exitStatus" == '0' ]; then
            ${pkgs.curl}/bin/curl -s -u "$(cat ''${CREDENTIALS_DIRECTORY}/ntfy-auth)" -H 't:${config.networking.hostName}' -H 'p:min' -H 'ta:white_check_mark' -d 'Postgres backup successful.' 'https://ntfy.artemislena.eu/borg'
          else
            ${pkgs.curl}/bin/curl -s -u "$(cat ''${CREDENTIALS_DIRECTORY}/ntfy-auth)" -H 't:${config.networking.hostName}' -H 'p:high' -H 'ta:warning' -d 'Postgres backup or integrity check failed.' 'https://ntfy.artemislena.eu/borg'
          fi
        '';
      };
    };
  };

  systemd.services = {
    postgresql.serviceConfig = {
      CapabilityBoundingSet = [ "" ];
      ExecPaths = [ "/nix/store" ];
      InaccessiblePaths = [ "/sys" "/run/dbus" "/run/user" "/run/nscd" config.microvm.stateDir ]; # Needs /dev/shm
      LockPersonality = true;
      NoExecPaths = [ "/" ];
      MemoryDenyWriteExecute = true;
      NoNewPrivileges = true;
      PrivateDevices = true;
      PrivateTmp = true;
      ProcSubset = "pid";
      ProtectClock = true;
      ProtectControlGroups = true;
      ProtectHome = true;
      ProtectHostname = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      ProtectProc = "invisible";
      ProtectSystem = "strict";
      RemoveIPC = true;
      RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
      RestrictNamespaces = true;
      RestrictRealtime = true;
      RestrictSUIDSGID = true;
      SystemCallArchitectures = "native";
      SystemCallFilter = [
        "@system-service"
        "~@privileged @aio @keyring @memlock @resources @setuid memfd_create" # Needs @ipc and @timer
      ];
    };

    borgbackup-job-postgres = {
      wantedBy = [ "borgbackup-job-backup.service" ];
      serviceConfig.LoadCredential = [
        "passphrase:/root/.borg-passphrase"
        "ntfy-auth:/etc/borg-ntfy-auth"
      ];
    };
  };

  # Adapted from https://nixos.org/manual/nixos/unstable/index.html#module-services-postgres-upgrading, built only when needed, thus commented out by default
  /*
  users.users.root.packages = [
    (pkgs.writeScriptBin "upgrade-pg" ''
      set -eux
      systemctl stop postgresql
      # Replace <new_version> and <old_version> with the appropriate numbers
      export NEWDATA="/var/lib/postgresql/<new_version>"
      export NEWBIN="${pkgs.postgresql_<new_version>}/bin"
      export OLDDATA="/var/lib/postgresql/<old_version>"
      export OLDBIN="${pkgs.postgresql_<old_version>}/bin"
      install -d -m 0700 -o postgres -g postgres "$NEWDATA"
      cd "$NEWDATA"
      su postgres -c "$NEWBIN/initdb ${builtins.toString config.services.postgresql.initdbArgs} -D $NEWDATA"
      su postgres -c "$NEWBIN/pg_upgrade --old-datadir $OLDDATA --new-datadir $NEWDATA --old-bindir $OLDBIN --new-bindir $NEWBIN $@"
    '')
  ];
  */
}
