{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    matrix-synapse-tools.synadm
  ];

  systemd = {
    services = {
      synapse-compress = rec {
        description = "Synapse state compressor";
        startAt = "weekly";
        requires = [ "postgresql.service" ];
        after = requires;
        path = [ config.services.postgresql.package ];
        preStart = ''
          # https://github.com/matrix-org/rust-synapse-compress-state/issues/78#issuecomment-1409932869
          echo "Starting script"

          psql matrix-synapse <<_EOF
          BEGIN;

          DELETE
          FROM state_compressor_state AS scs
          WHERE NOT EXISTS
            (SELECT *
             FROM rooms AS r
             WHERE r.room_id = scs.room_id);

          DELETE
          FROM state_compressor_state AS scs
          WHERE scs.room_id in
            (SELECT DISTINCT room_id
             FROM state_compressor_state AS scs2
             WHERE scs2.current_head IS NOT NULL
               AND NOT EXISTS
               (SELECT *
                FROM state_groups AS sg
                WHERE sg.id = scs2.current_head));

          DELETE
          FROM state_compressor_progress AS scp
          WHERE NOT EXISTS
            (SELECT *
             FROM state_compressor_state AS scs
             WHERE scs.room_id = scp.room_id);

          COMMIT;
          _EOF

          echo "Finished script"
        '';
        documentation = [ "https://github.com/matrix-org/rust-synapse-compress-state" ];
        serviceConfig = {
          User = "matrix-synapse";
          Group = "matrix-synapse";
          ExecStart = "${pkgs.matrix-synapse-tools.rust-synapse-compress-state}/bin/synapse_auto_compressor -p postgresql://matrix-synapse@/matrix-synapse?host=/var/run/postgresql -c 500 -n 100";
          Type = "oneshot";
          UMask = "0077";
          WorkingDirectory = "/var/lib/matrix-synapse";
          CapabilityBoundingSet = [ "" ];
          ExecPaths = [ "/nix/store" ];
          InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" config.microvm.stateDir ];
          LockPersonality = true;
          MemoryDenyWriteExecute = true;
          NoExecPaths = [ "/" ];
          NoNewPrivileges = true;
          PrivateDevices = true;
          PrivateTmp = true;
          PrivateUsers = true;
          ProcSubset = "pid";
          ProtectClock = true;
          ProtectControlGroups = true;
          ProtectHome = true;
          ProtectHostname = true;
          ProtectKernelLogs = true;
          ProtectKernelModules = true;
          ProtectKernelTunables = true;
          ProtectProc = "invisible";
          ProtectSystem = "strict";
          RemoveIPC = true;
          RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
          RestrictNamespaces = true;
          RestrictRealtime = true;
          RestrictSUIDSGID = true;
          StateDirectory = "matrix-synapse";
          SystemCallArchitectures = "native";
          SystemCallFilter = [
            "@system-service"
            "~@privileged @aio @keyring @memlock @resources @setuid @timer memfd_create" # Needs @ipc
          ];
        };
      };

      synapse-purge-media = rec {
        description = "Purge old media in the Synapse media store";
        startAt = "daily";
        requires = [ "matrix-synapse-worker-media.service" ];
        after = requires;
        path = with pkgs; [ curl ];
        script = ''
          TS="$(expr "$(date '+%s000')" - 604800000)" # Gives you the Unix timestamp of a week ago in ms
          curl -s --unix-socket /run/matrix-synapse/media --header "Authorization: Bearer $(cat /var/lib/matrix-synapse/admin_token)" -X POST "http://localhost/_synapse/admin/v1/purge_media_cache?before_ts=$TS" # Delete remote media
          curl -s --unix-socket /run/matrix-synapse/media --header "Authorization: Bearer $(cat /var/lib/matrix-synapse/admin_token)" -X POST "http://localhost/_synapse/admin/v1/media/artemislena.eu/delete?before_ts=$TS&size_gt=2000000" # Delete local media larger than 2MB
        '';
        serviceConfig = {
          DynamicUser = true;
          User = "matrix-synapse";
          Group = "matrix-synapse";
          Type = "oneshot";
        };
      };
    };

    timers = {
      synapse-compress.timerConfig.Persistent = true;
      synapse-purge-media.timerConfig.Persistent = true;
    };
  };
}
