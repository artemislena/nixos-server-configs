{ config, lib, ... }:

{
  imports = [
    ./misc.nix
    ./workers.nix
  ];

  services.matrix-synapse = {
    enable = true;
    withJemalloc = true;
    log.root.level = "WARNING";

    settings = rec {
      server_name = "artemislena.eu";
      public_baseurl = "https://matrix.artemislena.eu/";
      web_client_location = "https://element.artemislena.eu/";
      presence.enabled = false;
      signing_key_path = "/var/lib/matrix-synapse/${server_name}.signing.key";
      include_profile_data_on_invite = false;
      allow_public_rooms_without_auth = true;
      allow_public_rooms_over_federation = true;
      listeners = [
        {
          port = 8008;
          tls = false;
          resources = [ {
            names = [ "client" "federation" ];
          } ];
        }
        {
          path = "/run/matrix-synapse/main";
          mode = "666";
          resources = [ {
            names = [ "client" "federation" ];
          } ];
        }
      ];
      admin_contact = "mailto:admintexturally@artemislena.eu";
      redaction_retention_period = "1d";
      user_ips_max_age = "7d";
      retention = {
        enabled = true;
        default_policy.max_lifetime = "1y";
      };
      federation_client_minimum_tls_version = "1.2";
      caches = {
        global_factor = 2;
        cache_autotuning = {
          max_cache_memory_usage = "2G";
          target_cache_memory_usage = "1G";
          min_cache_ttl = "5m";
        };
      };
      max_upload_size = "2G";
      enable_registration = true;
      registration_requires_token = true;
      auto_join_rooms = [ "#blog:artemislena.eu" ];
      suppress_key_server_warning = true;
      bcrypt_rounds = 13;
      push.include_content = false;
      oidc_providers = [ {
        idp_id = "authelia";
        idp_name = "Authelia";
        idp_icon = "mxc://authelia.com/cKlrTPsGvlpKxAYeHWJsdVHI";
        issuer = "https://auth.artemislena.eu";
        client_id = "synapse";
        client_secret_path = "/etc/synapse/authelia_secret";
        scopes = [ "openid" "profile" ];
        allow_existing_users = true;
        user_mapping_provider.config = {
          localpart_template = "{{ user.preferred_username }}";
          display_name_template = "{{ user.name }}";
        };
      } ];
    };

    extraConfigFiles = [ "/etc/synapse/secrets.yaml" ];
  };

  systemd.services.matrix-synapse = rec {
    wants = [ "network-online.target" "podman-authelia.service" "caddy.service" ];
    after = wants ++ [ "postgresql.service" ];
  };
}
