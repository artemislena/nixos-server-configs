{ ... }:

{
  services.matrix-synapse = {
    configureRedisLocally = true;

    settings = {
      enable_media_repo = false;
      run_background_tasks_on = "background";
      update_user_directory_from_worker = "user_dir";
      pusher_instances = [ "pusher" ];
      federation_sender_instances = [ "federation_sender1" "federation_sender2" ];
      listeners = [ {
        path = "/run/matrix-synapse/replication";
        x_forwarded = false;
        resources = [ {
          names = [ "replication" ];
        } ];
      } ];
      instance_map = {
        main.path = "/run/matrix-synapse/replication";
        event_persister.path = "/run/matrix-synapse/event_persister";
      };
      stream_writers.events = "event_persister";
    };

    workers = let defaultWorkerListener = { path, resources ? [ "client" ] }: {
      path = path;
      mode = "666";
      resources = [ {
        names = resources;
      } ];
    }; in {
      federation_sender1 = {};
      federation_sender2 = {};
      pusher = {};
      background = {};
      media = {
        worker_app = "synapse.app.media_repository";
        worker_listeners = [ ( defaultWorkerListener {
          path = "/run/matrix-synapse/media";
          resources = [ "media" ];
        } ) ];
      };
      user_dir.worker_listeners = [ ( defaultWorkerListener { path = "/run/matrix-synapse/user_dir"; } ) ];
      encryption.worker_listeners = [ ( defaultWorkerListener { path = "/run/matrix-synapse/encryption"; } ) ];
      event_persister.worker_listeners = [ {
        path = "/run/matrix-synapse/event_persister";
        x_forwarded = false;
        resources = [ {
          names = [ "replication" ];
        } ];
      } ];
    };
  };
}
