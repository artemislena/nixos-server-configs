{ config, ... }:

{
  users = {
    users.syncplay = {
      isSystemUser = true;
      group = "syncplay";
    };
    groups.syncplay = {};
  };

  services = {
    syncplay = {
      enable = true;
      useACMEHost = "syncplay.artemislena.eu";
      saltFile = "/etc/syncplay/salt";
      extraArgs = [ "--isolate-room" ];
    };
  };

  systemd.services.syncplay.serviceConfig = {
    CapabilityBoundingSet = [ "" ];
    ExecPaths = [ "/nix/store" ];
    InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" config.microvm.stateDir ];
    LockPersonality = true;
    MemoryDenyWriteExecute = true;
    NoExecPaths = [ "/" ];
    PrivateDevices = true;
    PrivateUsers = true;
    ProcSubset = "pid";
    ProtectClock = true;
    ProtectControlGroups = true;
    ProtectHome = true;
    ProtectHostname = true;
    ProtectKernelLogs = true;
    ProtectKernelModules = true;
    ProtectKernelTunables = true;
    ProtectProc = "invisible";
    RestrictAddressFamilies = [ "AF_INET" "AF_INET6" ];
    RestrictNamespaces = true;
    RestrictRealtime = true;
    RestrictSUIDSGID = true;
    SystemCallArchitectures = "native";
    SystemCallFilter = [
      "@system-service"
      "~@privileged @aio @keyring @memlock @resources @setuid @timer memfd_create" # Needs @ipc
    ];
  };
}
