{ ... }:

{
  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "nontrivialwoolsaw@artemislena.eu";
      enableDebugLogs = false;
      dnsProvider = "desec";
      credentialsFile = "/etc/dns_tokens";
    };
    certs = {
      "bw.artemislena.eu" = {
        group = "caddy";
        extraDomainNames = [
          "*.bw.artemislena.eu"
          "fandom.artemislena.eu"
          "*.fandom.artemislena.eu"
        ];
        reloadServices = [ "caddy.service" ];
      };
    };
  };
}
