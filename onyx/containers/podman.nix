{ pkgs, lib, ... }:

{
  virtualisation = {
    oci-containers.backend = "podman";
    containers = {
      enable = true;
      containersConf.settings = {
        engine = {
          runtime = "runsc";
          runtimes.runsc = [ "${pkgs.gvisor}/bin/.runsc-wrapped" ];
        };
      };
    };
    podman = {
      autoPrune = {
        enable = true;
        flags = [ "--all" ];
      };
      defaultNetwork.settings = {
        ipv6_enabled = true;
        subnets = [
          {
            gateway = "10.88.0.1";
            subnet = "10.88.0.0/16";
          }
          {
            gateway = "2001:0db9::1";
            subnet = "2001:0db9::/112";
          }
        ];
      };
    };
  };

  systemd = {
    services = {
      podman.enable = false;
      podman-prune.requires = lib.mkForce [];
    };
    sockets.podman.enable = false;
    timers = {
      podman-auto-update = {
        wantedBy = [ "multi-user.target" ];
        timerConfig = {
          OnCalendar = [ "" "hourly" ];
          Persistent = true;
          RandomizedDelaySec = "20min";
        };
      };
      podman-prune.timerConfig.Persistent = true;
    };
  };

  services.borgbackup.jobs.backup.exclude = [ "/var/lib/containers" "/var/lib/cni" ];
}
