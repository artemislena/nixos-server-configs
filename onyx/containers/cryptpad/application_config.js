/*
 * You can override the configurable values from this file.
 * The recommended method is to make a copy of this file (/customize.dist/application_config.js)
   in a 'customize' directory (/customize/application_config.js).
 * If you want to check all the configurable values, you can open the internal configuration file
   but you should not change it directly (/common/application_config_internal.js)
*/
define(['/common/application_config_internal.js'], function (AppConfig) {
    // Example: If you want to remove the survey link in the menu:
    // AppConfig.surveyURL = "";

    // To inform users of the support ticket panel which languages your admins speak:
    AppConfig.supportLanguages = [ 'en', 'de' ];

    AppConfig.privacy = "https://pad.artemislena.eu/pad/#/2/pad/view/49ycCwI61u9JFX0WWss+oCwdMKj1i3B3V5xZVyEbQ68/";
    AppConfig.terms = "https://pad.artemislena.eu/code/#/2/code/view/CYlbPMXv3H8rgi-lUx14vNMVIcSRcV72JVvooM8igPE/present/";

    return AppConfig;
});
