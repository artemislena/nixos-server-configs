{ ... }:

{
  users = {
    users = let
      standardUser = { g, h ? "/var/empty", i }: {
        isSystemUser = true;
        uid = i;
        group = g;
        home = h;
        createHome = h != "/var/empty";
      };
    in {
      cryptpad = standardUser { g = "cryptpad"; h = "/var/lib/cryptpad"; i = 4001; };
      ffsend = standardUser { g = "ffsend"; h = "/var/lib/ffsend"; i = 5001; };
      authelia = standardUser { g = "authelia"; h = "/etc/authelia"; i = 5002; };
      gotosocial = standardUser { g = "gotosocial"; h = "/var/lib/gotosocial"; i = 5003; };
      mollysocket = standardUser { g = "mollysocket"; h = "/var/lib/mollysocket"; i = 5004; };
      draupnir = standardUser { g = "draupnir"; h = "/etc/draupnir"; i = 5005; };
      rimgo = standardUser { g = "rimgo"; i = 5007; };
      redlib = standardUser { g = "redlib"; i = 5008; };
    };
    groups = {
      cryptpad.gid = 4001;
      ffsend.gid = 5001;
      authelia.gid = 5002;
      gotosocial.gid = 5003;
      mollysocket.gid = 5004;
      draupnir.gid = 5005;
      rimgo.gid = 5007;
      redlib.gid = 5008;
    };
  };
}
