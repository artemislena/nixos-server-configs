{ config, pkgs, ... }:

let
  podmanIP = "10.88.0.1";
  onionDomain = "lpoaj7z2zkajuhgnlltpeqh3zyq7wk2iyeggqaduhgxhyajtdt2j7wad.onion";

  commonOptions = containerName: [
    "--label=io.containers.autoupdate=registry" "--label=PODMAN_SYSTEMD_UNIT=podman-${containerName}.service"
    "--security-opt=no-new-privileges" "--cap-drop=all" "--security-opt=proc-opts=hidepid=2" "--tmpfs=/sys:ro,noexec,nosuid,nodev" "--ipc=none"
    "--runtime-flag=platform=kvm" "--runtime-flag=oci-seccomp" "--runtime-flag=systemd-cgroup"
    "--runtime-flag=network=host" # Something is broken about gVisor's own networking
  ];

  readOnlyOptions = [ "--read-only" "--read-only-tmpfs=false" ];

  allowUdsOpen = [ "--runtime-flag=host-uds=open" ];
  allowUdsCreate = [ "--runtime-flag=host-uds=create" ];
in

{
  imports = [
    ./misc.nix
    ./podman.nix
    ./users.nix
  ];

  virtualisation.oci-containers.containers = {
    proxitok = {
      image = "ghcr.io/pablouser1/proxitok";
      ports = [ "127.0.0.1:8083:8080" ];
      extraOptions = commonOptions "proxitok" ++ allowUdsOpen ++ [ "--umask=0077" "--cap-add=setuid" "--cap-add=setgid" "--cap-add=chown" ];
      volumes = [
        "/var/cache/proxitok:/cache:noexec,nosuid,nodev"
        "${config.services.redis.servers.proxitok.unixSocket}:/run/redis/redis.sock:noexec,nosuid,nodev"
      ];
      environment = {
        LATTE_CACHE = "/cache";
        API_CACHE = "redis";
        REDIS_HOST = "/run/redis/redis.sock";
        REDIS_PORT = "-1";
        API_CHROMEDRIVER = "http://${podmanIP}:9515";
        APP_URL = "https://tok.artemislena.eu";
      };
      environmentFiles = [ /etc/proxitok_redis_password ];
      dependsOn = [ "chromedriver" ];
    };

    chromedriver = {
      image = "docker.io/zenika/alpine-chrome:with-chromedriver";
      extraOptions = commonOptions "chromedriver" ++ [ "--ipc=private" "--shm-size=1g" "--init" "--cap-add=sys_admin" "--security-opt=seccomp=/etc/proxitok/chrome.json" ]; # Seccomp profile (from https://github.com/jlandure/alpine-chrome/blob/master/chrome.json) should make it work without sys_admin capability, but did not work for us
      cmd = [ "--remote-debugging-pipe" ];
      ports = [ "${podmanIP}:9515:9515" ];
    };

    ytproxy = {
      image = "docker.io/1337kavin/ytproxy";
      user = "${toString config.users.users.caddy.uid}:${toString config.users.groups.caddy.gid}"; # Doesn't allow writes to UDS except by own user
      volumes = [ "/run/ytproxy:/app/socket" ];
      extraOptions = commonOptions "ytproxy" ++ readOnlyOptions ++ allowUdsCreate;
      environment.DISABLE_WEBP = "1";
    };

    draupnir = {
      image = "docker.io/gnuxie/draupnir:latest";
      ports = [ "127.0.0.1:8086:8080" ];
      extraOptions = commonOptions "draupnir" ++ readOnlyOptions ++ [ "--umask=0077" ];
      volumes = [ "/etc/draupnir:/data:rw,noexec,nosuid,nodev" ];
      user = "${toString config.users.users.draupnir.uid}:${toString config.users.groups.draupnir.gid}";
    };

    cryptpad = {
      image = "docker.io/cryptpad/cryptpad:version-2024.12.0";
      user = "${toString config.users.users.cryptpad.uid}:${toString config.users.groups.cryptpad.gid}";
      ports = [ "127.0.0.1:6000:3000" "127.0.0.1:6003:3003" ];
      volumes = [
        "/etc/nixos/containers/cryptpad/config.js:/cryptpad/config/config.js:ro,noexec,nosuid,nodev"
        "/etc/nixos/containers/cryptpad/application_config.js:/cryptpad/customize/application_config.js:ro,noexec,nosuid,nodev"
        "/var/lib/cryptpad/blob:/cryptpad/blob:noexec,nosuid,nodev"
        "/var/lib/cryptpad/block:/cryptpad/block:noexec,nosuid,nodev"
        "/var/lib/cryptpad/data:/cryptpad/data:noexec,nosuid,nodev"
        "/var/lib/cryptpad/files:/cryptpad/datastore:noexec,nosuid,nodev"
      ];
      environment = {
        CPAD_CONF = "/cryptpad/config/config.js";
        CPAD_INSTALL_ONLYOFFICE = "yes";
      };
      extraOptions = commonOptions "cryptpad";
    };

    breezewiki = {
      image = "quay.io/pussthecatorg/breezewiki:latest";
      ports = [ "127.0.0.1:10416:10416" ];
      environment = {
        bw_canonical_origin = "https://bw.artemislena.eu";
        bw_log_outgoing = "false";
      };
      extraOptions = commonOptions "breezewiki";
    };

    redlib = {
      image = "quay.io/redlib/redlib:latest";
      user = "${toString config.users.users.redlib.uid}:${toString config.users.groups.redlib.gid}";
      ports = [ "127.0.0.1:8087:8080" ];
      environment = {
        REDLIB_ROBOTS_DISABLE_INDEXING = "on";
        REDLIB_DEFAULT_POST_SORT = "new";
        REDLIB_DEFAULT_COMMENT_SORT = "new";
        REDLIB_DEFAULT_SHOW_NSFW = "on"; # Not showing makes posts simply disappear (rather than warning); could be confusing
        REDLIB_DEFAULT_BLUR_NSFW = "on";
        REDLIB_DEFAULT_USE_HLS = "on";
      };
      extraOptions = commonOptions "redlib" ++ readOnlyOptions ++ [ "--no-healthcheck" ];
    };

    ffsend = {
      image = "registry.gitlab.com/timvisee/send:latest";
      user = "${toString config.users.users.ffsend.uid}:${toString config.users.groups.ffsend.gid}";
      ports = [ "127.0.0.1:1443:1443" ];
      environment = {
        BASE_URL = "https://send.artemislena.eu";
        SEND_FOOTER_DMCA_URL = "https://artemislena.eu/contact";
        EXPIRE_TIMES_SECONDS = "3600,86400,172800,345600,604800";
        DEFAULT_DOWNLOADS = "3";
        DEFAULT_EXPIRE_SECONDS = "345600";
        REDIS_HOST = podmanIP;
        REDIS_PORT = toString config.services.redis.servers.ffsend.port;
        FILE_DIR = "/uploads";
      };
      environmentFiles = [ /etc/ffsend_redis_password ];
      volumes = [ "/var/lib/ffsend:/uploads:noexec,nosuid,nodev" ];
      extraOptions = commonOptions "ffsend" ++ readOnlyOptions ++ [ "--umask=0077" ];
    };

    authelia = {
      image = "ghcr.io/authelia/authelia";
      user = "${toString config.users.users.authelia.uid}:${toString config.users.groups.authelia.gid}";
      ports = [ "127.0.0.1:9091:9091" ];
      volumes = [
        "/etc/authelia:/config:noexec,nosuid,nodev" "/etc/nixos/containers/authelia.yml:/config/configuration.yml:ro,noexec,nosuid,nodev"
        "${config.services.redis.servers.authelia.unixSocket}:/var/run/redis/redis.sock:noexec,nosuid,nodev"
        "/var/run/postgresql:/var/run/postgresql:noexec,nosuid,nodev"
      ];
      environment = {
        AUTHELIA_IDENTITY_VALIDATION_RESET_PASSWORD_JWT_SECRET_FILE = "/config/JWT_SECRET";
        AUTHELIA_SESSION_SECRET_FILE = "/config/SESSION_SECRET";
        AUTHELIA_STORAGE_POSTGRES_PASSWORD_FILE = "/config/POSTGRES_PASSWORD";
        AUTHELIA_STORAGE_ENCRYPTION_KEY_FILE = "/config/STORAGE_ENCRYPTION_KEY";
        AUTHELIA_IDENTITY_PROVIDERS_OIDC_HMAC_SECRET_FILE = "/config/oidc/HMAC_SECRET";
        X_AUTHELIA_CONFIG_FILTERS = "template";
      };
      cmd = [ "authelia" ];
      extraOptions = commonOptions "authelia" ++ allowUdsOpen;
    };

    gotosocial = {
      image = "docker.io/superseriousbusiness/gotosocial";
      user = "${toString config.users.users.gotosocial.uid}:${toString config.users.groups.gotosocial.gid}";
      ports = [ "127.0.0.1:8081:8080" ];
      volumes = [ "/var/lib/gotosocial:/gotosocial/storage:noexec,nosuid,nodev" "/var/run/postgresql:/var/run/postgresql:noexec,nosuid,nodev" ];
      environment = {
        TZ = config.time.timeZone;
        GTS_LOG_LEVEL = "warn";
        GTS_LOG_CLIENT_IP = "false";
        GTS_HOST = "social.artemislena.eu";
        GTS_ACCOUNT_DOMAIN = "artemislena.eu";
        GTS_TRUSTED_PROXIES = "10.0.0.80";
        GTS_DB_ADDRESS = "/var/run/postgresql";
        GTS_DB_USER = "gotosocial";
        GTS_DB_DATABASE = "gotosocial";
        GTS_ACCOUNTS_REGISTRATION_OPEN = "false";
        GTS_MEDIA_VIDEO_MAX_SIZE = "104857600";
        GTS_MEDIA_DESCRIPTION_MIN_CHARS = "1";
        GTS_MEDIA_DESCRIPTION_MAX_CHARS = "1500";
        GTS_MEDIA_EMOJI_REMOTE_MAX_SIZE = "1048576";
        GTS_STATUSES_CW_MAX_CHARS = "200";
        GTS_OIDC_ENABLED = "true";
        GTS_OIDC_IDP_NAME = "authelia";
        GTS_OIDC_ISSUER = "https://auth.artemislena.eu";
        GTS_OIDC_CLIENT_ID = "gotosocial";
        GTS_OIDC_ADMIN_GROUPS = "admins";
      };
      environmentFiles = [ "/etc/gotosocial_secrets" ];
      dependsOn = [ "authelia" ];
      extraOptions = commonOptions "gotosocial" ++ allowUdsOpen ++ [ "--security-opt=apparmor=gotosocial"  "--read-only" "--umask=0077" ];
    };

    rimgo = {
      image = "codeberg.org/rimgo/rimgo";
      user = "${toString config.users.users.rimgo.uid}:${toString config.users.groups.rimgo.gid}";
      ports = [ "127.0.0.1:4000:3000" ];
      environment = {
        PRIVACY_COUNTRY = "Germany";
        PRIVACY_PROVIDER = "Vodafone Deutschland";
        PRIVACY_CLOUDFLARE = "false";
        PRIVACY_NOT_COLLECTED = "true";
      };
      extraOptions = commonOptions "rimgo" ++ readOnlyOptions;
    };

    mollysocket = {
      image = "ghcr.io/mollyim/mollysocket:latest";
      cmd = [ "server" ];
      ports = [ "127.0.0.1:8020:8020" ];
      user = "${toString config.users.users.mollysocket.uid}:${toString config.users.groups.mollysocket.gid}";
      volumes = [ "/var/lib/mollysocket:/data:noexec,nosuid,nodev" ];
      environment = {
        MOLLY_HOST = "0.0.0.0";
        MOLLY_ALLOWED_ENDPOINTS = ''["https://ntfy.artemislena.eu"]'';
        MOLLY_DB = "/data/db.sqlite";
      };
      environmentFiles = [ "/etc/ms_vapid_privkey" ];
      extraOptions = commonOptions "mollysocket" ++ readOnlyOptions ++ [ "--umask=0077" ];
    };
  };

  systemd = {
    services = let
      standardHardening = {
        LockPersonality = true;
        MemoryHigh = "150M";
        MemoryMax = "500M";
        RestrictRealtime = true;
        SystemCallArchitectures = "native";
      };
    in rec {
      podman-authelia = rec {
        requires = [ "postgresql.service" "redis-authelia.service" ];
        wants = [ "chronyd.service" ];
        after = requires ++ wants;
        serviceConfig = standardHardening;
      };
      podman-breezewiki.serviceConfig = standardHardening;
      podman-chromedriver.serviceConfig = standardHardening;
      podman-cryptpad.serviceConfig = standardHardening // {
        MemoryHigh = "3G";
        MemoryMax = "4G";
      };
      podman-draupnir.serviceConfig = standardHardening;
      podman-ffsend.serviceConfig = standardHardening;
      podman-gotosocial = rec {
        wants = [ "postgresql.service" "caddy.service" ];
        after = wants;
        path = [ pkgs.apparmor-parser ]; # https://github.com/NixOS/nixpkgs/pull/146969
        serviceConfig = standardHardening;
      };
      podman-mollysocket.serviceConfig = standardHardening;
      podman-proxitok.serviceConfig = standardHardening // {
        Restart = "always"; # Seems to crash sometimes?
      };
      podman-redlib.serviceConfig = standardHardening;
      podman-rimgo.serviceConfig = standardHardening;
      podman-ytproxy.serviceConfig = standardHardening;
    };

    tmpfiles.rules = [
      "d /run/ytproxy 0700 caddy caddy"
      "d /var/cache/proxitok 0700 nobody nogroup"
      "d /run/inv_sig_helper 0700 invidious invidious"
    ];
  };
}
