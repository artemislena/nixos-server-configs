{ config, pkgs, ... }:

{
  networking.firewall.interfaces.podman0.allowedTCPPorts = [
    6382 # ffsend Redis
    8009 # Pantalaimon
  ];

  security.apparmor.policies.gotosocial.profile = ''
    #include <tunables/global>

    profile gotosocial flags=(attach_disconnected, mediate_deleted) {
      #include <abstractions/base>
      #include <abstractions/nameservice>

      # Postgres socket
      network unix stream,
      network unix dgram,

      /gotosocial/gotosocial mrix,

      /gotosocial/{,**} r,
      owner /gotosocial/storage/** wk,

      # Allow GoToSocial to write logs
      /var/log/gotosocial/* w,

      /proc/sys/net/core/somaxconn r,
      /sys/kernel/mm/transparent_hugepage/hpage_pmd_size r,
      owner @{PROC}/@{pid}/cpuset r,

      # TCP / UDP network access
      network inet stream,
      network inet6 stream,
      network inet dgram,
      network inet6 dgram,

      # Allow GoToSocial to send signals to/receive signals from worker processes
      # Allow GoToSocial to receive signals from unconfined processes
      signal (receive) peer=unconfined,
      signal (send,receive) peer=gotosocial,

      # Needed to upload videos
      /tmp/** rw,
    }
  '';

  services.pantalaimon-headless.instances.mjolnir = {
    homeserver = "https://matrix.artemislena.eu";
    logLevel = "error";
    listenAddress = "10.88.0.1";
    extraSettings = {
      IgnoreVerification = true;
      UseKeyring = false;
    };
  };

  systemd.services = {
    rimgo-restart = {
      description = "Restart Rimgo";
      startAt = "hourly";
      serviceConfig = {
        ExecStart = "${pkgs.podman}/bin/podman --runtime-flag=platform=kvm --runtime-flag=oci-seccomp --runtime-flag=systemd-cgroup restart rimgo";
        Type = "oneshot";
      };
    };

    pantalaimon-mjolnir.serviceConfig = {
      UMask = "0077";
      InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" config.microvm.stateDir ];
      LockPersonality = true;
      PrivateUsers = true;
      ProcSubset = "pid";
      ProtectProc = "invisible";
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
      RestrictNamespaces = true;
      RestrictRealtime = true;
      SystemCallArchitectures = "native";
      SystemCallFilter = [
        "@system-service"
        "~@privileged @aio @ipc @keyring @memlock @resources @setuid @timer"
      ];
    };
  };
}
