{ pkgs, ... }:

{
  networking.firewall.allowedTCPPortRanges = [ {
    from = 9009;
    to = 9013;
  } ];

  users = {
    users.croc = {
      isSystemUser = true;
      group = "croc";
    };
    groups.croc = {};
  };

  systemd.services.croc = rec {
    description = "Croc relay server";
    documentation = [ "https://github.com/schollz/croc" ];
    wantedBy = [ "multi-user.target" ];
    wants = [ "network-online.target" ];
    after = wants;
    
    confinement = {
      enable = true;
      binSh = null;
    };

    serviceConfig = {
      User = "croc";
      Group = "croc";
      ExecStart = "${pkgs.croc}/bin/croc relay";

      CapabilityBoundingSet = [ "" ];
      ExecPaths = [ "/nix/store" ];
      InaccessiblePaths = [ "/dev/shm" "/sys" "/run" ];
      LockPersonality = true;
      MemoryDenyWriteExecute = true;
      NoExecPaths = [ "/" ];
      NoNewPrivileges = true;
      ProcSubset = "pid";
      ProtectClock = true;
      ProtectHostname = true;
      ProtectKernelLogs = true;
      ProtectProc = "invisible";
      RemoveIPC = true;
      RestrictAddressFamilies = [ "AF_INET" "AF_INET6" ];
      RestrictNamespaces = true;
      RestrictRealtime = true;
      RestrictSUIDSGID = true;
      SystemCallArchitectures = "native";
      SystemCallFilter = [
        "@system-service"
        "~@privileged @aio @keyring @memlock @setuid @timer memfd_create" # Needs @ipc and @resources
      ];
    };
  };
}
