{ lib, ... }:

{
  imports = [ ./shared/caddy.nix ];

  networking.firewall.allowedUDPPorts = [ 843 ];

  services.caddy = {
    globalConfig = ''
      # https://stackoverflow.com/questions/72411373/does-chrome-support-http-3-over-a-port-other-than-443
      servers :443 {
        protocols h1 h2
      }
      servers :843 {
        protocols h3
      }
    '';
    virtualHosts."http://" = {
      listenAddresses = [ "192.145.46.43" "2a03:4000:42:19d::2" ];
      logFormat = "output discard";
      extraConfig = ''
        header -Server
        @mail-acme {
          not host jitsi.artemislena.eu
          path /.well-known/acme-challenge/*
        }
        route {
          reverse_proxy @mail-acme http://10.0.1.1
          redir https://{host}{uri} permanent
        }
      '';
    };
  };

  systemd.services.caddy.serviceConfig.RestartPreventExitStatus = lib.mkForce "";
}
