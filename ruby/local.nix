{ config, ... }:

{
  imports = [
    ./wireguard.nix
    ./nat.nix
    ./croc.nix
    ./jitsi.nix
    ./sniproxy.nix
    ./caddy.nix
    ./shared/tarssh.nix
  ];

  networking = {
    hostName = "ruby";
    firewall.interfaces.ens3.allowedUDPPorts = [ 5183 ];
    interfaces.ens3 = {
      useDHCP = true;
      ipv6.addresses = [
        {
          address = "2a03:4000:42:19d::1";
          prefixLength = 64;
        }
        {
          address = "2a03:4000:42:19d::2";
          prefixLength = 64;
        }
      ];
      tempAddress = "disabled";
    };
  };

  services = {
    openssh.ports = [ 41395 ]; # This makes automated attacks infeasible and allows access from networks which block port 22
    qemuGuest.enable = true;
    chrony.extraConfig = "makestep 1 3"; # Not in LAN, but should have RTC battery
  };

  swapDevices = [ {
    device = "/dev/disk/by-partuuid/82aa410f-6ee9-413d-a737-d7cb64339274";
    randomEncryption = {
      enable = true;
      allowDiscards = true;
    };
  } ];

  services.borgbackup.jobs.backup = {
    repo = "ssh://ruby@borg.artemislena.eu:2222/./";
    startAt = "weekly"; # No need to do frequent backups of this one, it doesn't hold important persistent data
  };

  system.stateVersion = "24.05"; # Don't change unless you know what you're doing
}
