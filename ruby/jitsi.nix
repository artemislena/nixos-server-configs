{ config, pkgs, lib, ... }:

# Partially based on https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/services/web-apps/jitsi-meet.nix
# License for the original: https://github.com/NixOS/nixpkgs/blob/master/COPYING

with lib;

let
  cfg = config.services.jitsi-meet;

  # The configuration files are JS of format "var <<string>> = <<JSON>>;". In order to
  # override only some settings, we need to extract the JSON, use jq to merge it with
  # the config provided by user, and then reconstruct the file.
  overrideJs =
    source: varName: userCfg: appendExtra:
    let
      extractor = pkgs.writeText "extractor.js" ''
        var fs = require("fs");
        eval(fs.readFileSync(process.argv[2], 'utf8'));
        process.stdout.write(JSON.stringify(eval(process.argv[3])));
      '';
      userJson = pkgs.writeText "user.json" (builtins.toJSON userCfg);
    in (pkgs.runCommand "${varName}.js" { } ''
      ${pkgs.nodejs}/bin/node ${extractor} ${source} ${varName} > default.json
      (
        echo "var ${varName} = "
        ${pkgs.jq}/bin/jq -s '.[0] * .[1]' default.json ${userJson}
        echo ";"
        echo ${escapeShellArg appendExtra}
      ) > $out
    '');

  # Essential config - it's probably not good to have these as option default because
  # types.attrs doesn't do merging. Let's merge explicitly, can still be overriden if
  # user desires.
  defaultCfg = {
    hosts = {
      domain = cfg.hostName;
      muc = "conference.${cfg.hostName}";
      focus = "focus.${cfg.hostName}";
      jigasi = "jigasi.${cfg.hostName}";
    };
    bosh = "//${cfg.hostName}/http-bind";
    websocket = "wss://${cfg.hostName}/xmpp-websocket";

    fileRecordingsEnabled = true;
    liveStreamingEnabled = true;
  };

  extraJitsiHardening = {
    InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" ];
    PrivateUsers = true;
    ProcSubset = "pid";
    ProtectClock = true;
    ProtectKernelLogs = true;
    ProtectProc = "invisible";
    RemoveIPC = true;
    SystemCallArchitectures = "native";
    SystemCallFilter = [
      "@system-service"
      "~@privileged @aio @keyring @memlock @resources @setuid @timer memfd_create" # Needs @ipc
    ];
  };
in

{
  # https://github.com/NixOS/nixpkgs/pull/334638
  nixpkgs.config.permittedInsecurePackages = [
    "jitsi-meet-1.0.8043"
  ];

  services.jitsi-meet = {
    enable = true;
    hostName = "jitsi.artemislena.eu";
    config.prejoinConfig.enabled = true;
    interfaceConfig.SHOW_JITSI_WATERMARK = false;

    # Manual webserver configuration preferred
    caddy.enable = false;
    nginx.enable = false;
  };

  services.jitsi-videobridge.openFirewall = true;

  systemd.services = {
    jitsi-videobridge2.serviceConfig = extraJitsiHardening;
    jicofo.serviceConfig = extraJitsiHardening;
    prosody.serviceConfig = extraJitsiHardening // {
      CapabilityBoundingSet = [ "" ];
      ExecPaths = [ "/nix/store" ];
      LockPersonality = true;
      NoExecPaths = [ "/" ];
      NoNewPrivileges = true;
      ProtectSystem = "strict";
      RestrictAddressFamilies = [ "AF_INET" "AF_INET6" "AF_UNIX" ];
    };
  };

  services.caddy.virtualHosts = {
    "(jitsi)" = {
      logFormat = "output discard";
      extraConfig =
      let
        templatedJitsiMeet = pkgs.runCommand "templated-jitsi-meet" { } ''
          cp -R --no-preserve=all ${pkgs.jitsi-meet}/* .
          for file in *.html **/*.html ; do
            ${pkgs.sd}/bin/sd '<!--#include virtual="(.*)" -->' '{{ include "$1" }}' $file
          done
          rm config.js
          rm interface_config.js
          cp -R . $out
          cp ${overrideJs "${pkgs.jitsi-meet}/config.js" "config" (recursiveUpdate defaultCfg cfg.config) cfg.extraConfig} $out/config.js
          cp ${overrideJs "${pkgs.jitsi-meet}/interface_config.js" "interfaceConfig" cfg.interfaceConfig ""} $out/interface_config.js
          cp ./libs/external_api.min.js $out/external_api.js
        '';
      in ''
        header {
          Referrer-Policy no-referrer
          X-Content-Type-Options nosniff
          Strict-Transport-Security "max-age=63072000; includeSubDomains"
          Permissions-Policy "accelerometer=(), autoplay=(), camera=(self), clipboard-read=(), clipboard-write=(), cross-origin-isolated=(), display-capture=(self), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(self), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(self), midi=(), navigation-override=(), payment=(), picture-in-picture=(self), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), web-share=(), xr-spatial-tracking=()"
          Content-Security-Policy "default-src 'none'; img-src https: data:; media-src 'self' data:; connect-src 'self' wss://jitsi.artemislena.eu; manifest-src 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; script-src-attr 'none'; worker-src 'self' blob:; style-src 'self' 'unsafe-inline'; style-src-attr 'none'; font-src 'self'; base-uri 'self'; form-action 'none'; frame-ancestors 'none'"
          Access-Control-Allow-Origin "*"
          -Server
        }

        route {
          @ads path /ads.txt /app-ads.txt
          respond @ads "placeholder.example.com, placeholder, DIRECT, placeholder"
          @bot <<CEL
            header_regexp('User-Agent', '(?i)chatglm.spider|chatglm\\.cn|ai[0-9]bot|allenai\\.org')
            || header({'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/605.1.15 (KHTML, like Gecko; compatible; FriendlyCrawler/1.0) Chrome/120.0.6099.216 Safari/605.1.15',
            'User-Agent': 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; PerplexityBot/1.0; +https://docs.perplexity.ai/docs/perplexity-bot)',
            'User-Agent': 'nikto', 'User-Agent': 'sqlmap'})
            || path('*.php', '*.aspx', '/wp/*', '/wordpress*', '/wp-*')
            CEL
          redir @bot https://ash-speed.hetzner.com/10GB.bin

          # NixOS package doesn't include a manifest.json apparently; this is the one used by meet.jit.si
          respond /manifest.json `${builtins.toJSON {
            android_package_name = "org.jitsi.meet";
            prefer_related_applications = true;
            related_applications = [ {
              id = "org.jitsi.meet";
              platform = "chromeos_play";
            } ];
            short_name = "Jitsi Meet";
            name = "Jitsi Meet";
            icons = [
              {
                src = "static/pwa/icons/icon192.png";
                type = "image/png";
                sizes = "192x192";
              }
              {
                src = "static/pwa/icons/icon512.png";
                type = "image/png";
                sizes = "512x512";
              }
              {
                src = "static/pwa/icons/iconMask.png";
                sizes = "512x512";
                type = "image/png";
                purpose = "maskable";
              }
            ];
            start_url = "/";
            background_color = "#17A0DB";
            display = "standalone";
            scope = "/";
            theme_color = "#17A0DB";
          } }`

          @backend path /http-bind /xmpp-websocket
          handle @backend {
            reverse_proxy 127.0.0.1:5280 {
              transport http {
                compression off
              }
            }
          }
          handle {
            templates
            root * ${templatedJitsiMeet}
            try_files {path} /index.html
            file_server
            encode zstd gzip
          }
        }
      '';
    };
    "https://${cfg.hostName}" = {
      listenAddresses = [ "127.0.0.1" "2a03:4000:42:19d::2" ];
      logFormat = "output discard";
      extraConfig = ''
        import jitsi
        header Alt-Svc "h3=\":843\"; ma=2592000"
      '';
    };
    "https://${cfg.hostName}:843" = {
      listenAddresses = [ "192.145.46.43" "2a03:4000:42:19d::2" ];
      logFormat = "output discard";
      extraConfig = "import jitsi";
    };
  };
}
