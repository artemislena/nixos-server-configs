{ config, ... }:

{
  networking.nat = rec {
    enable = true;
    enableIPv6 = true;
    externalIP = "192.145.46.43";
    externalIPv6 = "2a03:4000:42:19d::1";
    internalInterfaces = [ "wg1" ];

    # Manual port forwarding setup, because NixOS's own is interface- rather than address-based
    extraCommands =
    let
      forwardIPv4 = port: "iptables -w -t nat -A nixos-nat-pre -p tcp -d ${externalIP} --dport ${toString port} -j DNAT --to-destination 10.0.1.1:${toString port}";
      forwardIPv6 = port: "ip6tables -w -t nat -A nixos-nat-pre -p tcp -d ${externalIPv6} --dport ${toString port} -j DNAT --to-destination [fc00:1::1]:${toString port}";
      forward = port: ''
        ${forwardIPv4 port}
        ${forwardIPv6 port}
      '';
    in ''
      ${forward 25}
      ${forward 465}
      ${forward 587}
      ${forward 993}
      ${forward 995}
      ${forward 4190}
      ${forwardIPv6 80}
      ${forwardIPv6 443}
    '';
  };
}
