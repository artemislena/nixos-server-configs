{ config, ... }:

{
  networking.firewall.allowedTCPPorts = [ 443 ];

  services.sniproxy = {
    enable = true;
    config = ''
      error_log {
        syslog daemon
        priority notice
      }

      listen 192.145.46.43:443 {
        protocol tls
        fallback 10.0.1.1:443
      }

      table {
        jitsi.artemislena.eu 127.0.0.1:443
        mail.artemislena.eu 10.0.1.1:443
        autoconfig.artemislena.eu 10.0.1.1:443
        autodiscover.artemislena.eu 10.0.1.1:443
      }
    '';
  };

  systemd.services.sniproxy.serviceConfig = {
    RestartSec = "10s"; # Fails after reboot for unknown reasons

    CapabilityBoundingSet = [ "CAP_NET_BIND_SERVICE" "CAP_SETUID" "CAP_SETGID" ];
    ExecPaths = [ "/nix/store" ];
    InaccessiblePaths = [ "/dev/shm" "/sys" "/run/dbus" "/run/user" "/run/nscd" ];
    LockPersonality = true;
    MemoryDenyWriteExecute = true;
    MemoryHigh = "10M";
    MemoryMax = "100M";
    NoExecPaths = [ "/" ];
    NoNewPrivileges = true;
    PrivateDevices = true;
    PrivateTmp = true;
    ProcSubset = "pid";
    ProtectClock = true;
    ProtectControlGroups = true;
    ProtectHome = true;
    ProtectHostname = true;
    ProtectKernelLogs = true;
    ProtectKernelModules = true;
    ProtectKernelTunables = true;
    ProtectProc = "invisible";
    ProtectSystem = "strict";
    RemoveIPC = true;
    RestrictAddressFamilies = [ "AF_INET" "AF_INET6" ];
    RestrictRealtime = true;
    RestrictSUIDSGID = true;
    SystemCallArchitectures = "native";
    SystemCallFilter = [
      "@system-service"
      "~@aio @chown @keyring @memlock @resources @timer memfd_create" # Needs @privileged and @ipc
    ];
  };
}
