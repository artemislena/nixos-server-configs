{ ... }:

{
  networking.wireguard.useNetworkd = false; # Can't get this working currently

  networking.wireguard.interfaces.wg1 = {
    ips = [ "10.0.1.2/24" "fc00:1::2/64" ];
    listenPort = 5183;
    privateKeyFile = "/etc/wireguard/private";
    peers = [ {
      publicKey = "ql5GVzEQ2jiiVvuXsS5dxI2jXo3EpQ7jzd6LacTg2wY=";
      presharedKeyFile = "/etc/wireguard/psk";
      allowedIPs = [ "10.0.1.0/24" "fc00:1::/64" ];
    } ];
  };
}
