{ config, pkgs, lib, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./local.nix
    ./shared/borg.nix
    ./shared/chrony.nix
    ./shared/environment.nix
    ./shared/hardening.nix
    ./shared/misc.nix
    ./shared/network.nix
    ./shared/nix.nix
    ./shared/ssh.nix
    ./shared/systemd-boot.nix
  ];

  # Reduce reboot downtimes and prevent system freezes, also see man systemd-system.conf
  systemd.watchdog = {
    rebootTime = lib.mkDefault "1s";
    runtimeTime = "30s";
  };

  system.autoUpgrade = {
    enable = true;
    allowReboot = true;
    dates = lib.mkDefault "18:00";
    randomizedDelaySec = "45min";
    flake = "/etc/nixos";
    flags = [
      "--update-input" "nixpkgs"
      "--commit-lock-file"
    ];
  };

  hardware.enableRedistributableFirmware = true; # Microcode updates
}
