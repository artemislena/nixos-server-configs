{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    lanzaboote = {
      url = "github:nix-community/lanzaboote/64d20cb2afaad8b73f4e38de41d27fb30a782bb5";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    microvm = {
      url = "github:astro/microvm.nix/1e746a8987eb893adc8dd317b84e73d72803b650";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, lanzaboote, microvm, ... }: {
    nixosConfigurations = {
      onyx = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";

        modules = [
          microvm.nixosModules.host
          lanzaboote.nixosModules.lanzaboote
          ./configuration.nix
          ./lanzaboote.nix
        ];
      };

      ruby = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ ./configuration.nix ];
      };
    };
  };
}
