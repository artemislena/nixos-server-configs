# NixOS server configs

The config files for our various NixOS servers, as well as a simple deploy script. Licensed under the Unlicense (see `UNLICENSE` file), except for
`podman/cryptpad/nginx.conf` (see the comment at the top of that file).

## Network/router/hardware dependent stuff
- Via for Mailcow server reaching its WireGuard endpoint
- Borg repo URL
- Desec update script
- MAC address of br0 (on Onyx)
- VPS (ruby): interface and NAT config, listening addresses for sniproxy and Caddy/Jitsi, WireGuard endpoint and custom route destination for Mailcow server
