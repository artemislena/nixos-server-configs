#!/bin/sh

set -e

if [ $# -lt 1 ]; then
    echo "Usage: $0 server_name [upgrade]"
    exit 1
fi


umask 022
chmod -R a+rX configuration.nix "$1" shared

echo "Deploying to $1 …"

scp -r flake.nix configuration.nix shared/ "$1"/* "root@$1:/etc/nixos/"

if [ "$2" = 'upgrade' ]; then
    ssh -T "root@$1" "cd /etc/nixos; nix flake update"
fi

ssh -T "root@$1" "nixos-rebuild switch"
